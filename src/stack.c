#include <stdint.h>
#include "stack.h"
#include "registers.h"
#include "config.h"

static uint16_t stack[C8_STACK_SIZE] = {0};

void C8_push_to_stack(uint16_t new_pc) {
  C8_inc_sp();
  stack[C8_get_sp()] = C8_get_pc();
  C8_set_pc(new_pc);
}

void C8_pop_stack() {
  C8_set_pc(stack[C8_get_sp()]);
  C8_dec_sp();
}
