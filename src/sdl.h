#ifndef __SDL_H
#define __SDL_H

#include <stdbool.h>
#include <SDL2/SDL.h>

/* these are the useful triggers that are returned by the
event handler function - c8_SDL_poll_for_events */
typedef enum trigger {
  QUIT_TRIGGER,
  NO_TRIGGER
} trigger;

/* typedef of the function that is executed in the main loop.
It must take arguments SDL_Window*, SDL_Renderer* and it must return void */
typedef void (*c8_main_loop_execution_function) (SDL_Window*, SDL_Renderer*);

/* gets the SDL Window based on title and screen size */
SDL_Window* C8_SDL_get_window(char* title, unsigned int width, unsigned int height);

/* polls for useful events and returns the latest trigger */
trigger C8_SDL_poll_for_events();

/* this is the core of the SDL stuff - creates and runs the main loop  */
void C8_start_main_loop(c8_main_loop_execution_function func);

#endif
