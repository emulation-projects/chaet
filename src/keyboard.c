#include "keyboard.h"
#include <stdint.h>
#include <stdbool.h>

static uint16_t keys = 0;

void C8_reset_keys() {
  keys = 0;
}

void C8_set_key(C8_KEY key) {
  keys |= 1 << key;
}

void C8_unset_key(C8_KEY key) {
  keys &= (~(1 << key));
}

bool C8_is_pressed(C8_KEY key) {
  return keys & (1 << key);
}
