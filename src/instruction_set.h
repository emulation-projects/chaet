#ifndef INSTRUCTION_SET_H
#define INSTRUCTION_SET_H
#include <stdint.h>
#include <stdbool.h>

/* execute the next instruction in the memory at PC  */
void C8_execute_next_instruction ();

/* get opcode index */
uint8_t C8_get_opcode_index (uint16_t ins);

/* return the first nibble after opcode nibble */
uint8_t C8_get_first_nible(uint16_t ins);

/* return the second nibble after opcode nibble */
uint8_t C8_get_second_nible(uint16_t ins);

/* return the first nibble after opcode nibble */
uint8_t C8_get_third_nible(uint16_t ins);

/* get address from instruction */
uint16_t C8_get_address (uint16_t ins);
#endif
