#include <stdint.h>

#ifndef __H_REGISTERS
#define __H_REGISTERS

/* sets the register with the given data
   Does not work on I since it's 16bit
 */
void C8_set_reg_u8(uint8_t r, uint8_t data);

/* sets the address register */
void C8_set_reg_i(uint16_t data);

/* return data in register specified by r */
uint8_t C8_get_reg_u8(uint8_t r);

/* return data in address register */
uint16_t C8_get_reg_i();

/* decrement delay register by 1 */
void C8_dec_delay_reg();

/* get data in delay register */
uint8_t C8_get_delay_reg();

/* set data in delay register */
void C8_set_delay_reg(uint8_t val);

/* decrement sound register by 1 */
void C8_dec_sound_reg();

/* set data in delay register */
void C8_set_sound_reg(uint8_t val);

/* get data in sound register */
uint8_t C8_get_sound_reg();

/* set program counter */
void C8_set_pc (uint16_t);

/* set stack pointer */
void C8_set_sp (uint8_t);

/* Increment stack pointer */
void C8_inc_sp ();

/* Decrement stack pointer */
void C8_dec_sp ();

/* Increment Program Counter */
void C8_inc_pc ();

/* get Program Counter */
uint16_t C8_get_pc();

/* get Stack Pointer */
uint8_t C8_get_sp();

void C8_print_registers();
#endif
