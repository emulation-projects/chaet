#ifndef __COLORS_H
#define __COLORS_H

#include <SDL2/SDL.h>

extern SDL_Color fg_color;
extern SDL_Color bg_color;

#endif
