#ifndef __CONFIG_H
#define __CONFIG_H

#define TITLE "CEAT - Chip-8 Emulator"
#define WIN_WIDTH 1024
#define WIN_HEIGHT 600

#define C8_WIDTH 64
#define C8_HEIGHT 32
#define C8_PIXEL_SIZE 15
#define C8_FPS 60
#define C8_MEM_SIZE 4096        /* in bits */
#define C8_START_POSITION 512
#define C8_STACK_SIZE 16


#define BG {24, 48, 48, 255}
#define FG {123, 113, 98, 255}

#endif
