#include <stdlib.h>
#include <time.h>

#include "instruction_set.h"
#include "registers.h"
#include "stack.h"
#include "draw.h"
#include "keyboard.h"
#include "memory.h"

/* Execute machine language subroutine at address NNN  */
static bool f_0nnn(uint16_t instruction) {
  return true;
}
/* Scroll down N lines */
static bool f_00cn (uint16_t instruction) {
  /* C8_scroll_down(instruction & 0x000f); */
  return true;
}

/* scroll right by 4 pixels */
static bool f_00fb (uint16_t instruction) {
  C8_scroll_right(4);
  return true;
}

/* scroll left by 4 pixels */
static bool f_00fc (uint16_t instruction) {
  C8_scroll_left(4);
  return true;
}

/* quit emulator */
static bool f_00fd (uint16_t instruction) {
  exit(0);
}

/* Clear the screen */
static bool f_00e0(uint16_t instruction) {
  printf("%x\n", instruction);
  C8_clear_screen();
  return true;
}
/* Return from a subroutine */
static bool f_00ee(uint16_t instruction) {
  C8_pop_stack();
  return true;
}
/* Jump to address NNN  */
static bool f_1nnn(uint16_t instruction) {
  C8_set_pc(C8_get_address(instruction));
  return false;
}
/* Execute subroutine starting at address NNN  */
static bool f_2nnn(uint16_t instruction) {
  C8_push_to_stack(C8_get_address(instruction));
  return false;
}
/* Skip the following instruction if the value of register VX equals NN  */
static bool f_3xnn(uint16_t instruction) {
  if (C8_get_reg_u8(C8_get_first_nible(instruction)) == (instruction & 0x00ff)) {
      C8_inc_pc();
    }
  return true;
}
/* Skip the following instruction if the value of register VX is not equal to NN  */
static bool f_4xnn(uint16_t instruction) {
  uint8_t vx = C8_get_reg_u8(C8_get_first_nible(instruction));
  uint8_t nn = instruction & 0x00ff;
  /* C8_print_registers(); */
  if (vx != nn) {
    C8_inc_pc();
  }
  /* C8_print_registers(); */
  return true;
}
/* Skip the following instruction if the value of register VX is equal to the value of register VY  */
static bool f_5xy0(uint16_t instruction) {
  uint8_t vx = C8_get_reg_u8(C8_get_first_nible(instruction)),
    vy = C8_get_reg_u8(C8_get_second_nible(instruction));
  if (vx == vy) {
    C8_inc_pc();
  }
  return true;
}
/* Store number NN in register VX  */
static bool f_6xnn(uint16_t instruction) {
  C8_set_reg_u8(C8_get_first_nible(instruction),
                (instruction & 0x00ff));
  return true;
}
/* Add the value NN to register VX  */
static bool f_7xnn(uint16_t instruction) {
  uint8_t x = C8_get_first_nible(instruction),
    vx = C8_get_reg_u8(x);
  C8_set_reg_u8(x, (instruction & 0x00ff) + vx);
  return true;
}
/* Store the value of register VY in register VX  */
static bool f_8xy0(uint16_t instruction) {
  C8_set_reg_u8(C8_get_first_nible(instruction),
                C8_get_reg_u8(C8_get_second_nible(instruction)));
  return true;
}
/* Set VX to VX OR VY  */
static bool f_8xy1(uint16_t instruction) {
  uint8_t vx = C8_get_reg_u8(C8_get_first_nible(instruction)),
    vy = C8_get_reg_u8(C8_get_second_nible(instruction));
  C8_set_reg_u8(C8_get_first_nible(instruction), vx | vy);
  return true;
}
/* Set VX to VX AND VY  */
static bool f_8xy2(uint16_t instruction) {
  uint8_t vx = C8_get_reg_u8(C8_get_first_nible(instruction)),
    vy = C8_get_reg_u8(C8_get_second_nible(instruction));
  C8_set_reg_u8(C8_get_first_nible(instruction), vx & vy);
  return true;
}
/* Set VX to VX XOR VY  */
static bool f_8xy3(uint16_t instruction) {
  uint8_t vx = C8_get_reg_u8(C8_get_first_nible(instruction)),
    vy = C8_get_reg_u8(C8_get_second_nible(instruction));
  C8_set_reg_u8(C8_get_first_nible(instruction),
                vx ^ vy);
  return true;
}
/* Add the value of register VY to register VX
   Set VF to 01 if a carry occurs
   Set VF to 00 if a carry does not occur */
static bool f_8xy4(uint16_t instruction) {
  uint8_t vx = C8_get_reg_u8(C8_get_first_nible(instruction)),
    vy = C8_get_reg_u8(C8_get_second_nible(instruction));
  uint16_t sum = vx + vy;
  if (sum > 255) {
    C8_set_reg_u8(0x0f, 1);
  } else {
    C8_set_reg_u8(0x0f, 0);
  }
  C8_set_reg_u8(C8_get_first_nible(instruction), sum & 0x00ff);
  return true;
}
/* Subtract the value of register VY from register VX
   Set VF to 00 if a borrow occurs
   Set VF to 01 if a borrow does not occur */
static bool f_8xy5(uint16_t instruction) {
  uint8_t vx = C8_get_reg_u8(C8_get_first_nible(instruction)),
    vy = C8_get_reg_u8(C8_get_second_nible(instruction));
  if (vx > vy) {
    C8_set_reg_u8(0x0f, 1);
  } else {
    C8_set_reg_u8(0x0f, 0);
  }
  C8_set_reg_u8(C8_get_first_nible(instruction), vx - vy);
  return true;
}
/* Store the value of register VY shifted right one bit in register VX¹
   Set register VF to the least significant bit prior to the shift
   VY is unchanged */
static bool f_8xy6(uint16_t instruction) {
  uint8_t x = C8_get_first_nible(instruction),
    y = C8_get_second_nible(instruction),
    vy = C8_get_reg_u8(y);
  C8_set_reg_u8(x, vy >> 1);
  C8_set_reg_u8(0x0f, vy & 1);
  return true;
}
/* Set register VX to the value of VY minus VX
   Set VF to 00 if a borrow occurs
   Set VF to 01 if a borrow does not occur */
static bool f_8xy7(uint16_t instruction) {
  uint8_t x = C8_get_first_nible(instruction),
    y = C8_get_second_nible(instruction),
    vx = C8_get_reg_u8(x),
    vy = C8_get_reg_u8(y);
  int sub = vy - vx;
  C8_set_reg_u8(x, sub);
  C8_set_reg_u8(0x0f, (vy > vx));
  return true;
}
/* Store the value of register VY shifted left one bit in register VX¹
   Set register VF to the most significant bit prior to the shift
   VY is unchanged */
static bool f_8xye(uint16_t instruction) {
  uint8_t x = C8_get_first_nible(instruction),
    y = C8_get_second_nible(instruction),
    vy = C8_get_reg_u8(y);
  C8_set_reg_u8(x, vy << 1);
  C8_set_reg_u8(0x0f, vy >> 7);
  return true;
}
/* Skip the following instruction if the value of register VX is not equal to the value of register VY  */
static bool f_9xy0(uint16_t instruction) {
  uint8_t vx = C8_get_reg_u8(C8_get_first_nible(instruction)),
    vy = C8_get_reg_u8(C8_get_second_nible(instruction));
  if (vx != vy) {
    C8_inc_pc();
  }
  return true;
}
/* Store memory address NNN in register I  */
static bool f_annn(uint16_t instruction) {
  C8_set_reg_i(C8_get_address(instruction));
  return true;
}
/* Jump to address NNN + V0  */
static bool f_bnnn(uint16_t instruction) {
  C8_set_pc((uint16_t)C8_get_reg_u8(0) + C8_get_address(instruction));
  return true;
}
/* Set VX to a random number with a mask of NN  */
static bool f_cxnn(uint16_t instruction) {
  static unsigned int seed = 0;
  uint16_t mask = instruction & 0x00ff;
  uint8_t fn = C8_get_first_nible(instruction);
  seed += time(0);              /* change the seed  */
  double random_num = (rand_r(&seed) % 255) & mask;
  C8_set_reg_u8(fn, random_num);
  return true;
}
/* Draw a sprite at position VX, VY with N bytes of sprite data starting at the address stored in I
   Set VF to 01 if any set pixels are changed to unset, and 00 otherwise */
static bool f_dxyn(uint16_t instruction) {
  /* C8_print_registers(); */
  uint8_t x = C8_get_reg_u8((C8_get_first_nible(instruction))),
    y = C8_get_reg_u8((C8_get_second_nible(instruction))),
    n = C8_get_third_nible(instruction);
  if (C8_draw_sprite(x, y, C8_get_reg_i(), n)) {
    /* collision occured */
    C8_set_reg_u8(0x0f, 1);
  } else {
    C8_set_reg_u8(0x0f, 0);
  }
  return true;
}
/* Skip the following instruction if the key corresponding to the hex value currently stored in register VX is pressed */
static bool f_ex9e(uint16_t instruction) {
  if (C8_is_pressed(C8_get_first_nible(instruction))) {
    C8_inc_pc();
  }
  return true;
}
/* Skip the following instruction if the key corresponding to the hex value currently stored in register VX is not pressed */
static bool f_exa1(uint16_t instruction) {
  if (!C8_is_pressed(C8_get_first_nible(instruction))) {
    C8_inc_pc();
  }
  return true;
}
/* Store the current value of the delay timer in register VX  */
static bool f_fx07(uint16_t instruction) {
  C8_set_reg_u8(C8_get_first_nible(instruction),  C8_get_delay_reg());
  return true;
}
/* Wait for a keypress and store the result in register VX  */
static bool f_fx0a(uint16_t instruction) {
  if (C8_is_pressed(C8_get_first_nible(instruction)))
    return true;
  return false;
}
/* Set the delay timer to the value of register VX  */
static bool f_fx15(uint16_t instruction) {
  C8_set_delay_reg(C8_get_reg_u8(C8_get_first_nible(instruction)));
  return true;
}
/* Set the sound timer to the value of register VX  */
static bool f_fx18(uint16_t instruction) {
  C8_set_sound_reg(C8_get_reg_u8(C8_get_first_nible(instruction)));
  return true;
}
/* Add the value stored in register VX to register I  */
static bool f_fx1e(uint16_t instruction) {
  C8_set_reg_i(C8_get_reg_i() + C8_get_reg_u8(C8_get_first_nible(instruction)));
  return true;
}
/* Set I to the memory address of the sprite data corresponding to the hexadecimal digit stored in register VX  */
static bool f_fx29(uint16_t instruction) {
  C8_set_reg_i(C8_get_first_nible(instruction) * 5); /* 5 is the size(bytes) of each internal sprite  */
  return true;
}
/* Store the binary-coded decimal equivalent of the value stored in register VX at addresses I, I + 1, and I + 2  */
static bool f_fx33(uint16_t instruction) {
  uint8_t fn = C8_get_first_nible(instruction),
    reg_val = C8_get_reg_u8(fn);
  C8_set_memory_cell(C8_get_reg_i(), reg_val / 100);
  C8_set_memory_cell(C8_get_reg_i() + 1, (reg_val / 10) % 10);
  C8_set_memory_cell(C8_get_reg_i() + 2, (reg_val % 100) % 10);
  return true;
}
/* Store the values of registers V0 to VX inclusive in memory starting at address I
   I is set to I + X + 1 after operation  */
static bool f_fx55(uint16_t instruction) {
  uint8_t fn = C8_get_first_nible(instruction);
  for (int j = 0; j <= fn; ++j) {
    C8_set_memory_cell(C8_get_reg_i(), C8_get_reg_u8(j));
    C8_set_reg_i(C8_get_reg_i() + 1);
  }
  return true;
}
/* Fill registers V0 to VX inclusive with the values stored in memory starting at address I
   I is set to I + X + 1 after operation  */
static bool f_fx65(uint16_t instruction) {
  for (int reg_index = 0; reg_index <= C8_get_first_nible(instruction); ++reg_index) {
    C8_set_reg_u8(reg_index, C8_get_data_at_location(C8_get_reg_i()));
    C8_set_reg_i(C8_get_reg_i() + 1);
  }
  return true;
}


/* Return the next instruction in the memory at PC  */
static uint16_t C8_get_next_instruction () {
  uint8_t msb = C8_get_data_at_location(C8_get_pc()),
    lsb = C8_get_data_at_location(C8_get_pc() + 1);
  uint16_t ins = msb;
  return (ins << 8) | lsb;
}

static bool __parse_instruction_0(uint16_t ins) {
  if ((ins & 0x00F0) == 0x00C0)
    return f_00cn(ins);
  switch (ins & 0x0fff) {
  case 0x00fb:
    return f_00fb(ins);
  case 0x00fc:
    return f_00fc(ins);
  case 0x00fd:
    return f_00fd(ins);
  case 0x00e0:
    return f_00e0(ins);
  case 0x00ee:
    return f_00ee(ins);
  default:
    return f_0nnn(ins);
  }
}
static bool __parse_instruction_1(uint16_t ins) {
  return f_1nnn(ins);
}
static bool __parse_instruction_2(uint16_t ins) {
  return f_2nnn(ins);
}
static bool __parse_instruction_3(uint16_t ins) {
  return f_3xnn(ins);
}
static bool __parse_instruction_4(uint16_t ins) {
  return f_4xnn(ins);
}
static bool __parse_instruction_5(uint16_t ins) {
  return f_5xy0(ins);
}
static bool __parse_instruction_6(uint16_t ins) {
  return f_6xnn(ins);
}
static bool __parse_instruction_7(uint16_t ins) {
  return f_7xnn(ins);
}
static bool __parse_instruction_8(uint16_t ins) {
  switch (ins & 0x000f) {
  case 0x0000:
    return f_8xy0(ins);
  case 0x0001:
    return f_8xy1(ins);
  case 0x0002:
    return f_8xy2(ins);
  case 0x0003:
    return f_8xy3(ins);
  case 0x0004:
    return f_8xy4(ins);
  case 0x0005:
    return f_8xy5(ins);
  case 0x0006:
    return f_8xy6(ins);
  case 0x0007:
    return f_8xy7(ins);
  default:
    return f_8xye(ins);
  }
}
static bool __parse_instruction_9(uint16_t ins) {
  return f_9xy0(ins);
}
static bool __parse_instruction_a(uint16_t ins) {
  return f_annn(ins);
}
static bool __parse_instruction_b(uint16_t ins) {
  return f_bnnn(ins);
}
static bool __parse_instruction_c(uint16_t ins) {
  return f_cxnn(ins);
}
static bool __parse_instruction_d(uint16_t ins) {
  return f_dxyn(ins);
}
static bool __parse_instruction_e(uint16_t ins) {
  if ((ins & 1) == 1) {
    return f_exa1(ins);
  }
  return f_ex9e(ins);
}
static bool __parse_instruction_f(uint16_t ins) {
  switch (ins & 0x00ff) {
  case 0x0007:
    return f_fx07(ins);
  case 0x000a:
    return f_fx0a(ins);
  case 0x0015:
    return f_fx15(ins);
  case 0x0018:
    return f_fx18(ins);
  case 0x001e:
    return f_fx1e(ins);
  case 0x0029:
    return f_fx29(ins);
  case 0x0033:
    return f_fx33(ins);
  case 0x0055:
    return f_fx55(ins);
  default:
    return f_fx65(ins);
  }
}

/* array of functions related to the code */
static bool (*parser_funcs[])(uint16_t) = {
  __parse_instruction_0,    __parse_instruction_1,
  __parse_instruction_2,    __parse_instruction_3,
  __parse_instruction_4,    __parse_instruction_5,
  __parse_instruction_6,    __parse_instruction_7,
  __parse_instruction_8,    __parse_instruction_9,
  __parse_instruction_a,    __parse_instruction_b,
  __parse_instruction_c,    __parse_instruction_d,
  __parse_instruction_e,    __parse_instruction_f
};

static bool __execute_instruction(uint16_t instruction) {
  uint8_t first_nibble = (instruction & 0xf000) >> 12;
  return (parser_funcs[first_nibble])(instruction);
}

void C8_execute_next_instruction () {
  /* step 1 - Get the next instruction */
  uint16_t ins = C8_get_next_instruction();
  if (ins) {
    /* Step 2 - Figure out which instruction this is */
    /* step 3 - execute the instruction */

    bool increment_pc = __execute_instruction(ins);
    /* step 4 - increment the PC if necessary */
    if (increment_pc) {
      C8_inc_pc();
    }
    /* printf("INS - %x\n", ins); */
    /* C8_print_registers(); */
    /* fgetc(stdin); */
  }
}

uint8_t C8_get_opcode_index (uint16_t ins) {
  return (ins & 0xF000) >> 12;
}

uint8_t C8_get_first_nible(uint16_t ins) {
  return (ins & 0x0F00) >> 8;
}

uint8_t C8_get_second_nible(uint16_t ins) {
  return (ins & 0x00F0) >> 4;
}

uint8_t C8_get_third_nible(uint16_t ins) {
  return (ins & 0x000f);
}

uint16_t C8_get_address (uint16_t ins) {
  return ins & 0x0FFF;
}
