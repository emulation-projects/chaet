#ifndef __MAIN_LOOP_H
#define __MAIN_LOOP_H

#include <SDL2/SDL.h>

void main_loop (SDL_Window* window,
                SDL_Renderer* renderer);

#endif
