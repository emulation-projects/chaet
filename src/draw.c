#include <SDL2/SDL.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "config.h"
#include "memory.h"
#include "colors.h"

static uint64_t grid[C8_HEIGHT] = {0};

void C8_scroll_down (int n) {
  uint64_t last_row = grid[C8_HEIGHT - 1];
  for (int grid_c = C8_HEIGHT; grid_c > 0; --grid_c) {
    grid[grid_c] = grid[grid_c-1];
  }
  grid[0] = last_row;
}

void C8_scroll_left(int n) {
  for (int col = 0; col < C8_HEIGHT; ++col) {
    grid[col] = grid[col] >> n;
  }
}

void C8_scroll_right(int n) {
  for (int col = 0; col < C8_HEIGHT; ++col) {
    grid[col] = grid[col] << n;
  }
}

static bool check_bit (uint64_t *grid,
                       int x, int y) {
  uint64_t row = grid[y];
  return row & ((uint64_t)1) << x;
}

bool C8_add_pixel(int x, int y) {
  uint64_t new_row = grid[y] | ((uint64_t) 1) << x;
  bool collision = new_row ^ grid[y];
  grid[y] = new_row;
  return collision;
}

void C8_Draw_grid(SDL_Renderer* renderer,
                  int n_cells_rows,
                  int n_cells_cols,
                  SDL_Rect cell) {
  int x = cell.x;
  for (int col = 0; col < n_cells_cols; ++col) {
    for (int row = 0; row < n_cells_rows; ++row) {
      cell.x += cell.w;
      if (check_bit(grid, row, col)) {
        SDL_SetRenderDrawColor(renderer, fg_color.r, fg_color.g, fg_color.b, fg_color.a);
        SDL_RenderFillRect(renderer, &cell);
      } else {
        SDL_SetRenderDrawColor(renderer, bg_color.r, bg_color.g, bg_color.b, bg_color.a);
        SDL_RenderDrawRect(renderer, &cell);
      }
    }
    cell.y += cell.h; cell.x = x;
  }
}

bool C8_draw_sprite (int x, int y, uint16_t start_addr, size_t n) {
  bool collision = false;
  uint8_t row = 0;

  for (size_t row_counter = 0; row_counter < n; ++row_counter) {

    row = C8_get_data_at_location(start_addr + row_counter);
    /* reverse the byte
       credit: https://graphics.stanford.edu/~seander/bithacks.html#ReverseByteWith64BitsDiv
    */
    row = (row * 0x0202020202ULL & 0x010884422010ULL) % 1023;
    for (int col = 0; col < 8; col++) {
      if (row & 1 << col){
        collision |= C8_add_pixel(x + col, y);
      }
    }
    y++;
  }
  return collision;
}

bool C8_draw_internal_sprite (uint8_t index, int x, int y) {
  return C8_draw_sprite(x, y, index * 5, 5);
}


void C8_clear_screen() {
  for (int row = 0; row < C8_HEIGHT; ++row) {
    grid[row] = 0;
  }
}
