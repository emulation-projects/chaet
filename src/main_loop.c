#include <SDL2/SDL.h>
#include "draw.h"
#include "colors.h"
#include "registers.h"
#include "config.h"
#include "memory.h"
#include "instruction_set.h"

static SDL_Rect border_rect = {
  .x=((WIN_WIDTH - (C8_PIXEL_SIZE * C8_WIDTH)) / 2) - 25,
  .y=((WIN_HEIGHT - (C8_PIXEL_SIZE * C8_HEIGHT)) / 2) - 25,
  .w=C8_WIDTH * C8_PIXEL_SIZE + 50,
  .h=C8_HEIGHT * C8_PIXEL_SIZE + 50};

static SDL_Rect cell = {
  .x=(WIN_WIDTH - (C8_PIXEL_SIZE * C8_WIDTH)) / 2,
  .y=(WIN_HEIGHT - (C8_PIXEL_SIZE * C8_HEIGHT)) / 2,
  .w=C8_PIXEL_SIZE,
  .h=C8_PIXEL_SIZE
};


void main_loop (SDL_Window* window,
                SDL_Renderer* renderer) {

  /* decrement the delay and sound registers on every call */
  C8_dec_delay_reg();
  C8_dec_sound_reg();

  C8_execute_next_instruction();
  /* C8_print_memory(); */


  SDL_SetRenderDrawColor(renderer, fg_color.r, fg_color.g, fg_color.b, fg_color.a);
  SDL_RenderDrawRect(renderer, &border_rect);
  SDL_SetRenderDrawColor(renderer, bg_color.r, bg_color.g, bg_color.b, bg_color.a);
  C8_Draw_grid(renderer, C8_WIDTH, C8_HEIGHT, cell);
}
