#ifndef __DRAW_H
#define __DRAW_H

#include <SDL2/SDL.h>
#include <stdbool.h>

/* draw a grid of rectangles
   new_grid - used to perform xor with old grid,
   if xor is true then collision occured.
   n_cells_rows - No. of cells in a row
   n_cells_cols - No. of cells in a columns
   size - SDL_Rect size of a single cell

Returns true if collision occured, false otherwise.
 */
void C8_Draw_grid(SDL_Renderer*,
                  int n_cells_rows,
                  int n_cells_cols,
                  SDL_Rect cell);

/* adds pixel to grid and returns true if collision occurs,
returns false if collision does not occur. */
bool C8_add_pixel(int x, int y);

/* scroll down n rows */
void C8_scroll_down (int n);

/* scroll left n lines */
void C8_scroll_left(int n);

/* scroll right n lines */
void C8_scroll_right(int n);

/* an internal sprite is 8x5 in size. This function draws
the sprite to the grid at position (x, y). */
bool C8_draw_internal_sprite (uint8_t index,
                           int x, int y);

/* Draw a sprite of size n(bytes) starting at address [start_addr]
   at position (x, y)*/
bool C8_draw_sprite (int x, int y, uint16_t start_addr, size_t n);

/* clear teh c8 screen */
void C8_clear_screen();
#endif
