#include "registers.h"
#include "config.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

static uint8_t data_regs[18] = {0};
static uint16_t addr_reg = 0;
static uint8_t delay_reg = 0;
static uint8_t sound_reg = 0;
static uint8_t stack_pointer = 0;
static uint16_t program_counter = C8_START_POSITION;

void C8_set_pc (uint16_t new_pc) {
  program_counter = new_pc;
}

void C8_set_sp (uint8_t new_sp) {
  stack_pointer = new_sp;
}

void C8_inc_sp () {
  stack_pointer++;
}

void C8_dec_sp () {
  stack_pointer--;
}

void C8_set_reg_u8(uint8_t r, uint8_t data) {
  if (r < 16) {
    data_regs[r] = data;
  }
}

uint8_t C8_get_reg_u8(uint8_t r) {
  return data_regs[r];
}

void C8_set_reg_i(uint16_t data) {
  addr_reg = data;
}

uint16_t C8_get_reg_i() {
  return addr_reg;
}

void C8_dec_delay_reg() {
  if (delay_reg > 0) delay_reg--;
}

uint8_t C8_get_delay_reg() {
  return delay_reg;
}

void C8_dec_sound_reg() {
  if (sound_reg > 0) sound_reg--;
}

uint8_t C8_get_sound_reg() {
  return sound_reg;
}

uint16_t C8_get_pc() {
  return program_counter;
}

uint8_t C8_get_sp() {
  return stack_pointer;
}

void C8_inc_pc () {
  program_counter += 2;
}

void C8_set_delay_reg(uint8_t val) {
  delay_reg = val;
}

void C8_set_sound_reg(uint8_t val) {
  sound_reg = val;
}


void C8_print_registers() {
  printf("data registers:\n");
  for (int reg = 0; reg < 16; ++reg) {
    printf("%x \t", data_regs[reg]);
  }
  printf("\n");
  printf("I\tdelay\tsound\tSP\tPC\n");
  printf("%x\t%x\t%x\t%x\t%x", addr_reg, delay_reg, sound_reg, stack_pointer, program_counter);
  printf("\n");
}
