#ifndef __MEMORY_H
#define __MEMORY_H
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "config.h"

/* one cell of memory is 1 byte  */
typedef uint8_t mem_cell_t;


/* load program of size n to memory */
void C8_load_program(mem_cell_t*, size_t);

/* load the internal sprites into memory starting at position 0 */
void C8_load_internal_sprites();

/* load program from file */
void C8_load_program_from_file(FILE* file);

/* clears the memory to 0 */
void C8_clear_memory();

/* set the value of memory at position "pos" to value "data" */
void C8_set_memory_cell(size_t pos, mem_cell_t data);

/* get data from memory at pos */
mem_cell_t C8_get_data_at_location (size_t pos);

void C8_print_memory();
#endif
