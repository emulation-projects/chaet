#include <stdio.h>
#include <stdbool.h>

#include "sdl.h"
#include "main_loop.h"
#include "memory.h"
#include "registers.h"
#include "instruction_set.h"

int main(int argc, char** argv)
{
  if (argc > 1){
    FILE* in_file = fopen(argv[1], "r");
    C8_load_internal_sprites();
    C8_load_program_from_file(in_file);
    C8_start_main_loop(main_loop);
  }
  return 0;
}
