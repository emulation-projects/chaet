#ifndef __KEYBOARD_H
#define __KEYBOARD_H

#include <stdbool.h>

typedef enum {
  K_0,
  K_1,
  K_2,
  K_3,
  K_4,
  K_5,
  K_6,
  K_7,
  K_8,
  K_9,
  K_A,
  K_B,
  K_C,
  K_D,
  K_E,
  K_F
} C8_KEY;

void C8_reset_keys();

void C8_set_key(C8_KEY key);

void C8_unset_key(C8_KEY key);

bool C8_is_pressed(C8_KEY key);

#endif
