#include <SDL2/SDL.h>
#include <stdio.h>

#include "sdl.h"
#include "config.h"
#include "colors.h"
#include "keyboard.h"

SDL_Window* C8_SDL_get_window(char* title, unsigned int width, unsigned int height) {
  if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
      fprintf(stderr, "Could not initialize SDL2: %s\n", SDL_GetError());
      return NULL;
    }
  SDL_Window *window = SDL_CreateWindow(title,
                                        SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED,
                                        width, height, 0);
  if(!window)
    {
      fprintf(stderr, "Could not create window: %s\n", SDL_GetError());
      return NULL;
    }
  return window;
}

trigger C8_SDL_poll_for_events() {
  SDL_Event e;
  int key_p;
  while(SDL_PollEvent(&e) > 0)
    {
      switch(e.type)
        {
        case SDL_KEYDOWN:
          key_p = e.key.keysym.sym;
          if (key_p >= 48 && key_p <= 57) {
            C8_set_key(key_p - 48);
          } else if (key_p >= 97 && key_p <= 102) {
            C8_set_key(key_p - 97 + 10);
          }
          break;
        case SDL_QUIT:
          return QUIT_TRIGGER;
        }
    }
  return NO_TRIGGER;
}

void C8_start_main_loop(c8_main_loop_execution_function func) {
  SDL_Window* window = C8_SDL_get_window(TITLE, WIN_WIDTH, WIN_HEIGHT);
  SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);;
  bool running = true;
  bool render_frame = true;
  unsigned int start_time = SDL_GetTicks();
  unsigned int time_diff = round(1000/C8_FPS);

  /* main loop */
  while (running) {
    C8_reset_keys();
    trigger trig = C8_SDL_poll_for_events();
    if (trig == QUIT_TRIGGER){
      running = false;
    }

    /* set bool render_frame every 1/<C8_FPS>th of a second */
    if (SDL_GetTicks() > start_time + time_diff){
      start_time = SDL_GetTicks();
      render_frame = true;
    } else {
      render_frame = false;
      SDL_Delay(SDL_GetTicks() - start_time);
    }
    if (render_frame) {
      /* render all the stuff */
      SDL_SetRenderDrawColor(renderer, bg_color.r, bg_color.g, bg_color.b, bg_color.a);
      SDL_RenderClear(renderer);
      if (func != NULL){
        func(window, renderer);
      }
      SDL_RenderPresent(renderer);
    }
  }

  /* cleanup */
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
}
