#ifndef __STACK_H
#define __STACK_H
#include <stdint.h>

/* add PC to stack */
void C8_push_to_stack(uint16_t);

/* pop PC from stack  */
void C8_pop_stack();

#endif
