SRCS  = src/main.c\
src/sdl.c\
src/main_loop.c\
src/draw.c\
src/colors.c\
src/registers.c\
src/memory.c\
src/stack.c\
src/keyboard.c\
src/instruction_set.c

CC = gcc
OPTS = `sdl2-config --libs --cflags`
DEBUG= -g

chaet: $(SRCS)
	mkdir -p build
	$(CC) $(OPTS) $(DEBUG) $^ -o build/$@

clean:
	rm -r build/ || true
